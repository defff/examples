#!/usr/bin/env python3
'''
https://www.sreality.cz/api/cs/v2/estates?category_main_cb=2&category_type_cb=1&locality_country_id=112&locality_region_id=14&no_auction=1

https://www.sreality.cz/api/cs/v2/estates?category_main_cb=2&category_type_cb=1&czk_price_summary_order2=0%7C8000000&locality_region_id=14&no_auction=1

estates => rodinne_domy
category_main_cb=2 => 2=rodinne_domy; 1=byty
category_type_cb=1 => 1=prodej; 2=pronajem
locality_country_id=112 => 112=okres
region_id=14 => jihomoravsky kraj
no_auction=1 => nechci aukce
czk_price_summary_order2=0%... => castka
sort=0 => nejnovejsi

'''

import csv
import requests
import math

cena = "price_summary_order2=0%7C8000000&"
region = "locality_region_id=14&"
prodej = "category_type_cb=1&"
domy = "category_main_cb=2&"
akce = "no_auction=1&"
vyber = "sort=0"

soubor = "/home/def/sreality/domky.csv"
zacatek_adresy = "https://www.sreality.cz/detail/prodej/dum/rodinny/"

cela_url = "https://www.sreality.cz/api/cs/v2/estates?"+domy+prodej+cena+region+akce+vyber

def zprava_tlgrm(zprava):
    bot_token = 'secret_token_do_not_tell:)'
    bot_id = 'even_more_secret_id'
    bot_text = 'https://api.telegram.org/bot' + bot_token + '/sendMessage?chat_id=' + bot_id + '&parse_mode=Markdown&text=' + zprava

    response = requests.get(bot_text)

    return response.json()

def posbirej_data():
  r = requests.get(cela_url)
  if r.status_code == 200:
    data = r.json()
    pocet_zaznamu = data['result_size']
    pocet_stranek = math.ceil(pocet_zaznamu / 20)
  
    with open(soubor,'w') as csv_descriptor:
      csv_writer = csv.writer(csv_descriptor)
      for i in range(pocet_stranek):
        nova_url = cela_url+"&page="+str(i)
        r_new = requests.get(nova_url)
        data_new = r_new.json()

        for i in data_new['_embedded']['estates']:
          csv_writer.writerow([i['name'],zacatek_adresy+str(i['seo']['locality'])+'/'+str(i['hash_id']),i['locality'],i['price'],i['hash_id']])
#posbirej_data()


def checkni_novy_zaznam():
  with open(soubor, 'r') as csv_descriptor:
    reader = csv.DictReader(csv_descriptor)
    ID = [row['ID'] for row in reader]
  
  with open(soubor, 'a+') as csv_descriptor:
    writer = csv.writer(csv_descriptor)
    nova_url = cela_url+"&page=1"
    r_new = requests.get(nova_url)
    data_new = r_new.json()
    for i in data_new['_embedded']['estates']:
      if str(i['hash_id']) not in ID:
        writer.writerow([i['name'],zacatek_adresy+str(i['seo']['locality'])+'/'+str(i['hash_id']),i['locality'],i['price'],i['hash_id']])
        zprava_tlgrm(zacatek_adresy+str(i['seo']['locality'])+'/'+str(i['hash_id']))
checkni_novy_zaznam()

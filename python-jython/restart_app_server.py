'''
 handle the restart of AppServer gracefully
 with terminate option (so not that gracefully)
 Author Karel 'def' Vampola
'''

#theCell = AdminControl.getCell()
import re

allServers = AdminConfig.list('Server').split('\n')
appServers = AdminTask.listServers('[-serverType APPLICATION_SERVER ]').split('\n')
for appServer in appServers:
   singleAppServer = appServer.split('(')
   for server in allServers:
      matchServerName = re.match('^'+singleAppServer[0]+'.*', server)
      if matchServerName:
         singleServer = server.split('(')
         singleNode = server.split('/')
         print "stoppping "+singleServer[0]+ " on " + singleNode[3] + " please wait"
         AdminControl.stopServer(singleServer[0], singleNode[3], 'terminate')

         print "server "+singleServer[0]+ " on " + singleNode[3] + " STOPPED"
         print "starting "+singleServer[0]+ " on " + singleNode[3] + " please wait"

         AdminControl.startServer(singleServer[0], singleNode[3])
         print "DONE"

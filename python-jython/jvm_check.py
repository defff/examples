import time

'''
jython app which monitors usage of HEAP of application JVM application servers
'''

f = open('/tmp/output.txt','a')
msg_members = ['msg_member1','msg_member2']
applicationServers = AdminTask.listServers('[-serverType APPLICATION_SERVER ]').split('\n')
for oneServer in applicationServers:
  singleServer = oneServer.split('(')[0]
  if singleServer not in msg_members:
    nameJVM = AdminControl.completeObjectName('type=JVM,process='+singleServer+',*')
    #it is necessary to create objects for the JVM & PERF; otherwise
    #arg can't be coerced to javax.management.ObjectName
    objectJVM = AdminControl.makeObjectName(nameJVM)
    perfInfo = AdminControl.completeObjectName('type=Perf,process='+singleServer+',*')
    objectPerf = AdminControl.makeObjectName(perfInfo)
    jvmStats = AdminControl.invoke_jmx(objectPerf,"getStatsObject",[objectJVM,java.lang.Boolean('false')],['javax.management.ObjectName','java.lang.Boolean'])A
    #and finaly get the statistics about the JVM
    wholeHeap = jvmStats.getStatistic('HeapSize').getCurrent()
    consumedHeap = jvmStats.getStatistic('UsedMemory').getCount()
    cpuUsage = jvmStats.getStatistic('ProcessCpuUsage').getCount()
    allStats=float(consumedHeap)/float(wholeHeap)*100
    print >>f,time.strftime('%x-%X'), "The usage of HEAP is ",int(allStats), "% for server ", singleServer," which is ", consumedHeap
f.close()

#!/usr/bin/env python3

import pandas
import yaml
import os, sys
import re

inventory_yaml = 'inventory.yml'

'''
script manipulates csv input and produces yaml file
Example of input is in INV folder

at the moment the csv always contains column "groups", the structure of CSV file is constant
each group can own multiple servers and each server can be member of multiple groups
servers withou groups are ignored [ I will probably change this ... sometimes, somewhere ... ]
I always wanted to play with pandas ... so test on a production data : )
Karel 'def' Vampola
'''

def create_yaml_inventory():
    group_dictionary = {}
    unique_groups = []
    try:
        soubor = sys.argv[1]
        #soubor = "INV/examples.csv"
        file_exists = os.path.isfile(soubor)
        if file_exists:
            csv_data = pandas.read_csv(soubor, index_col=None)
            csv_data.dropna(inplace = True)
            csv_groups = csv_data['groups'].str.split(',')

            for row in csv_groups:
                for value in row:
                    if value.strip() not in unique_groups:
                        unique_groups.append(value.strip())

            for one_group in unique_groups:  
                unique_data = csv_data['groups'].str.contains(fr'\b{one_group}\b',regex=True,case=False)
                #groups are dictionary within a dictionary ( each group holds 1 to N servers with its variables)
                group_dictionary[one_group] = []
                if unique_data.any():
                    #remove the 'groups' column from csv file
                    group_dictionary[one_group].append(csv_data.drop('groups', axis=1).loc[unique_data].to_dict(orient='row'))
                    #print(csv_data.loc[unique_data].to_dict(orient='row'))

            with open(inventory_yaml,'w') as yaml_descriptor:
                disable_aliases = yaml.dumper.SafeDumper
                disable_aliases.ignore_aliases = lambda self, data: True
                yaml.dump(group_dictionary,yaml_descriptor, Dumper=disable_aliases)
    except IndexError as errrr:
        print('Seems the input csv is not in place!', errrr)

create_yaml_inventory()

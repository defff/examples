import re
import java.io as jio
import java.util as jutil

'''
restart application(s) connected accross the whole cluster/cell
Karel 'def' Vampola
'''

file_with_app_name = jio.FileInputStream('./variables.conf')
properties = jutil.Properties()
properties.load(file_with_app_name)


application_to_restart = properties.getProperty('NAME_OF_APP')
cell_manager = AdminConfig.list('Cell')
cell_name = cell_manager.split('(')[0]
all_nodes = AdminConfig.list('Node', cell_manager).split('\n')
all_servers = AdminTask.listServers('[-serverType APPLICATION_SERVER ]').split('\n')

for one_node in all_nodes:
  one_node_name = AdminConfig.showAttribute(one_node, 'name')
  #all_servers = AdminControl.queryNames('type=Server,cell='+cell_name+',node='+one_node_name+',*').split('\n')
  for one_server in all_servers:
    app_server = one_server.split('(')[0]
    all_managers = AdminControl.queryNames('cell='+cell_name+',node='+one_node_name+',type=ApplicationManager,process='+app_server+',*').split('\n')
    all_apps = AdminControl.queryNames('type=Application,cell='+cell_name+',node='+one_node_name+',process='+app_server+',*').split('\n')
    for one_manager in all_managers:
      if one_manager != '':
        for one_application in all_apps:
          match_the_app = re.search(application_to_restart, one_application)
          match_the_manager = re.search(app_server, one_application)
          if match_the_app and match_the_manager:
            print 'stopping application '+application_to_restart+' on an application server named '+app_server
            print '----------------------------------------------------------------------------------------'
            AdminControl.invoke(one_manager, 'stopApplication', application_to_restart)
            print 'starting the application  '+application_to_restart+' on a application server named '+app_server 
            print '----------------------------------------------------------------------------------------'
            AdminControl.invoke(one_manager, 'startApplication', application_to_restart)


#!/bin/sh

#script check if:
#__ docker is installed -> installs it if not & runs it
#__ checks if the docker image 'comparator' exists -> if not wgets the dockerfile and builds it
#__ asks users for a dir where he stores lopr.csv + file with issues -> this dir is mounted to container
#__ executes the python script in container -> DONE

#####################################################################
# created by Karel 'def' Vampola
#####################################################################
rpm -qa | grep -i docker 

if [ $? != 0 ]; then
  sudo yum -y install docker
  if [ $? != 0 ]; then
    printf "Installation of docker failed, please install it manually\n"
    exit 1
  fi
fi

sudo systemctl start docker

if [ $? != 0 ]; then
  printf "Docker didn't started properly, please check logs and start it manually\n"
  exit 2
else
  eval sudo systemctl status docker | grep Active | grep running
  if [ $? != 0 ]; then
    printf "Docker is not running properly, please validate logs\n"  
    exit 3
  fi
fi

eval sudo docker images | grep comparator

if [ $? != 0 ]; then
  mkdir -p /tmp/comparator
  cd /tmp/comparator

  wget -O Dockerfile "https://raw.github.ibm.com/karel-vampola/compare/master/Dockerfile?token=AAFA1PEMSKKQ38iV36Zpv2Q1-Op7Oemvks5ddeI3wA%3D%3D"
  
  if [ $? != 0 ]; then
    printf "something horribly failed, please download files from github manually\n"
    exit 4
  fi
  
  wget -O comparator.py "https://raw.github.ibm.com/karel-vampola/compare/master/comparator.py?token=AAFA1EZVEi92TzVusmLzVJXiviV2Q-HEks5ddeJjwA%3D%3D"
                         
  
  if [ $? != 0 ]; then
    printf "something horribly failed, please download files from github manually\n"
    exit 5
  fi
  sudo docker build -t comparator .
fi

read -p 'Please provide path where you have stored "lopr & issues file": ' "userDirectory"

if [ ! -d "${userDirectory}" ]; then
  printf "Directory '${userDirectory}' doesn't exits, script will exit\n"
  exit 6
fi


read -p 'Please provide a name of LOPR csv file: ' "loprFile"

if [ ! -e "${userDirectory}/${loprFile}" ]; then
  printf "The file '${userDirectory}/${loprFile}' does not exist, script will exit\n"
  exit 7
fi

#/home/def/Desktop/GIT/tests/

read -p 'Please provide name of file containg list of issues: ' "issuesFile"

if [ ! -e "${userDirectory}/${issuesFile}" ]; then
  printf "The '${userDirectory}/${issuesFile}'  file does not exist, script will exit\n"
  exit 7
fi

#docker run --rm -it -v ${userDIR_withFiles}:/mnt/:Z comparator python3 /tmp/comparator.py "/mnt/LOPR_PATH" "/mnt/ISSUES_PATH"

sudo docker run --rm -t -v "${userDirectory}":/mnt/:Z comparator python3 /tmp/comparator.py "/mnt/${loprFile}" "/mnt/${issuesFile}"


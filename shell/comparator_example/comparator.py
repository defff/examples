#!/usr/local/bin/python3
import csv
import sys
import os


class colors:
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    END = '\033[0m'

if len(sys.argv) != 3:
    print(colors.WARNING + ' You must provide paths to "LOPR and File with findings" files in csv format as an arguments' + colors.END)
    print(colors.OKGREEN + ' Example: ' + colors.END)
    print("./comparator.py /home/USER/Desktop/LOPR.csv /home/USER/Downloads/ISSUES.csv")
    sys.exit(0)
else:
    check_csv_files = (sys.argv[1:3])
    for onecsv in check_csv_files:
        _,fileExtension = os.path.splitext(onecsv)
        if fileExtension != '.csv':
            print(colors.WARNING + ' The ' + onecsv + ' must be a CSV [comma separated] file! with .csv extension' + colors.END)

complete_scope = sys.argv[1]
file_issues = sys.argv[2]
generated_file = '/mnt/output_for_middleware.csv'
        
def server_names_lopr():
    with open(complete_scope,mode='r',encoding='UTF-8') as lopr:
        lopr_rows = csv.reader(lopr)
        next(lopr_rows)
        return [row[-3].lower() for row in lopr_rows]

def server_names_issue_list():
    with open(file_issues,mode='r',encoding='UTF-8') as issues:
        issue_rows = csv.reader(issues)
        next(issue_rows)
        return [row for row in issue_rows if row[2].lower() in server_names_lopr()]

def write_it_to_csv():
    with open(generated_file,mode='w',encoding='UTF-8') as output:
        writer = csv.writer(output,delimiter=',')
        for row in server_names_issue_list():
            writer.writerow(row)

write_it_to_csv()

function stop_remote 
{  
   $cluster2=(Get-WmiObject -Class Win32_Process -ComputerName 1.1.1.1 | Where-Object {$_.CommandLine -match 'Cluster?\d' -and $_.Caption -eq 'java.exe'})
   $cluster2.Terminate()
   
   
   $win_services_remote=(Get-WmiObject -Class Win32_Service -Computer 1.1.1.1 | Where-Object {$_.Name -match "IBMWAS*"})
   $remote_PID=$win_services_remote.ProcessID
   $win_services_remote.StopService()

   $win_services_remote=(Get-WmiObject -Class Win32_Service -Computer 1.1.1.1 | Where-Object {$_.Name -match "IBMWAS*"})
   
   if ($win_services_remote.State -ne "Stopped") {
      Stop-Process $remote_PID -Force
   }
}

function start_remote
{
   $win_services_remote=(Get-WmiObject -Class Win32_Service -Computer 1.1.1.1 | Where-Object {$_.Name -match "IBMWAS*"})

   if ($win_services_remote.State -ne "Running") {
      $win_services_remote.StartService()
   }
}


function stop_local
{
   $cluster1=(Get-WmiObject -Class Win32_Process | Where-Object {$_.CommandLine -match 'Cluster?\d' -and $_.Caption -eq 'java.exe'})
   $cluster1.Terminate()
   
   
   $win_services_local=(Get-WmiObject -Class Win32_Service | Where-Object {$_.Name -match "IBMWAS*"})

   foreach ($loc_service in $win_services_local) {
      $loc_service.StopService()
   }
   
   $win_updated_local=(Get-WmiObject -Class Win32_Service | Where-Object {$_.Name -match "IBMWAS*" -and $_.State -ne "Stopped"})

   if ($win_updated_local.Count > 0) {
      foreach ($updated_local in $win_updated_local) {
         $updated_pid=$updated_local.ProcessID
            Stop-Process $updated_pid -Force
      }
   }
}

function start_local
{
   $win_services_local=(Get-WmiObject -Class Win32_Service | Where-Object {$_.Name -match "IBMWAS*"})
   
   foreach ($loc_service in $win_services_local) {
      if ($loc_service.State -ne "Running") {
         $loc_service.StartService()
      }
   }
}


stop_remote
start_remote
Invoke-Expression -Command 'AppServer\bin\wsadmin.bat -f .\pystart.py'

stop_local
start_local
Invoke-Expression -Command 'AppServer\bin\wsadmin.bat -f .\pystart.py'

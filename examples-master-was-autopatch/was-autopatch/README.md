# <Automatic Installation Manager updates>

# # Description

- The whole documentation is in the docs section
- The idea was to develop custom module which will provide easy to use interface for users who never updated any product which uses Installation Manager and which can be pushed to multiple servers at once using the Ansible interface
- Even though that the most know product is probably WebSphere, it can be used for any application which uses it (Netcool Omnibus, Predective insight and others)
- It is assumed the product is already stopped, each application has a different stop procedure, it should not be considered valid to call e.g. "stopServer.sh" from Websphere perspective because these scripts hangs when there are too many JMS in queue or the threadpool is filled. 
- It should be noted that only products which reuses JVM runtime should be stopped for pathich activity (e.g. the IBM's IHS doesn't resuse runtime and can be patched online)
- The role "stop_WAS_processes" contains jython script which stops WAS processes based on the {{ ansible_nodename }}, to use this role it is assumed that the WAS $ENV are properly configured and the hostname can be retrieved from WAS ServerIndex object

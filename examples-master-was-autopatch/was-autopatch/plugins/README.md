# info about the module

This directory contains a custom module "iim_updater". The module is used for validation of repositories for IBM Installation Manager(IIM).
It checks XML content of already installed packages, checks and validate if new updates can be installed on the top of already existing packages
and provides the same checks for interim fixes. It provides a dict() with results.

The "most known" product which uses the IIM is a WebSphere, however this module can be used to extract and provide data for any application which was installed by it, which makes it versitale.
Only the standart python library is used due to a bit unfortunate situation with libraries on AIX. Therefore the xml.etree was used instead of lxml(or any module which would require the lxml).
Some of the functionality (e.g. reading of zip files) can be achieved by the Installation Manager itself, but the overall idea is to speed up the process and call IIM only for the installation. It provides quic and understandable interface for users who never used IIM.


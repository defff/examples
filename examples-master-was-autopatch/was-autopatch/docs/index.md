# <Automatic Installation Manager updates>

# Documentation

This collection provides a custom build module "iim_updater.py" located in plugins/modules section.
It provides the "standard" module documentaion which can be called via ansible-doc iim_updater if the module would reside in 'Ansible modules' directory or used as a collection.

It allows any user who is not anyhow familiar with the structure of any IIM product (websphere, netcool ..) to provide several paths to updates and the module will handle all logic instead of user.

For simplicity reasons the main_example.yml playbook alongisde with several roles was developed.
The main_example.yml is located in the playbooks section.
Since everything is supposed to be a dict() ( :) ), the playbooks just calls facts which are pushed from the iim_updater module.

The main_example.yml should be considered 'proof-of-concept' ready solution, it was tested on various WAS installation.

The role "stop_WAS_processes" contains jython script which stops WAS processes based on the {{ ansible_hostname }}, to use this role it is assumed that the WAS $ENV are properly configured and the hostname can be retrieved from WAS ServerIndex object, if it is impossible to retrieve the $ENV from the object the role wouldn't stop any WAS process.
The role is not included by default in the playbooks.


```
module: iim_updater 
description:
     - The M(iim_updater) module automatically installs updates for components of Installation Manager. It validates XML files, compares the result, updates only packages which are installed and possible to update (compares its version) and calls imcl commands only for update. Even though that the "path" to a package with an update is not a mandatory option, it is expected that either the 'path_to_update_iim_xml' or the 'path_to_iim_interim_location' exists; it would be impossible to update anything without at least one of the paths.

options:
  path_to_iim_xml:
    description:
        This is the path to installed.xml file created by The "IBM's Installation Manager.
        By default it is stored in $HOME directory of a user who performed the installation.
        With the exception of user root and custom specification of a path.
    required: true
  path_to_update_iim_xml:
    description:
        This is a path to a repository which holds the repository.xml. This file is provided by a package for update (assuming offline update).
        Files are usually several gigs in size and split into multiple zip files.
        This path must point to "repository.xml" which is extracted from above zips.
    required: false
  path_to_iim_interim_location:
    description:
      This is a path to a ZIP file for the interim FIX. These files are usually quick fixes for CVEs and are only few MB in size.
    required: false
  path_iim_location:
    description:
      Path to the location of Installation Manager executable imcl command
    required: true
  admin_user:
    description:
      The user who performed the installation of packages via Installation Manager.
      This is a check which validates if "become_user" == admin_user. Otherwise the Installation Manager
      would fail or corrupt already installed files.
    required: true

author:
    - "Karel 'def' Vampola"
```

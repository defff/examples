import java.lang.System as jsys
#default '\n'
line_separator = jsys.getProperty('line.separator')

get_cells = AdminConfig.list('Cell').split('line_separator')
def gather_node_names(all_cells):
    node_names = []
    for one_cell in all_cells:
        get_server_indexes = AdminConfig.list('ServerIndex').split('\n')
        for one_server_index in get_server_indexes:
            server_name = AdminConfig.show(one_server_index, 'hostName').split(' ')[1][:-1]
            id_node_name = one_server_index.split('/')[3].split('|')[0]
            #check if the argv is as same as hostName stored in WAS XML and collect nodes
            #only nodes/servers connected to hostName will be stopped
            #so other parts of cluster remains untouched
            if len(sys.argv) == 1 and server_name == sys.argv[0]:
                node_names.append(id_node_name)

    #now the dmgr node will have a string 'CellManager' within it
    #it is desired to stop dmgr as a last element, so moving its node to the end of list
    for sorted_node in node_names:
        if sorted_node.find('CellManager') >= 0:
            node_names.pop(node_names.index(sorted_node))
            node_names.append(sorted_node)

    return node_names

def gather_severs_connected_to_node(all_cells):
    gather_servers = []
    #check if the list is NOT empty
    if gather_node_names(get_cells):
        for one_cell in all_cells:
            name_of_cell = AdminConfig.showAttribute(one_cell, 'name')
            for one_node in gather_node_names(get_cells):
                all_servers = AdminControl.queryNames('type=Server,cell=' + name_of_cell + ',node=' + one_node + ',*').split(line_separator)
                for one_server in all_servers:
                    gather_servers.append(one_server)
        for one_server in gather_servers:
            #assuming that the one_server.split(',')[-1] process type is DeploymentManager
            #and in such case it should be stopped at last
            #therefore puting the process value at the end of the list
            if one_server.split(',')[-1] == 'processType=DeploymentManager':
                gather_servers.pop(gather_servers.index(one_server))
                gather_servers.append(one_server)
    return gather_servers

def stop_servers(stop_managed_servers):
    #check if the LIST is NOT empty
    if stop_managed_servers:
        for one_node in gather_node_names(get_cells):
            for one_server_id in stop_managed_servers: 
                if one_node == one_server_id.split(',')[3].split('=')[1]:
                    if one_server_id.split(',')[-1] == 'processType=UnManagedProcess':
                        print "stopping unmanaged process"
                        AdminControl.stopServer(one_server_id, one_node)
                        sleep(10)
                    else:
                        sleep(5)
                        print "stopping the " one_server_id + "one node " + one_node
                        AdminControl.stopServer(one_server_id, one_node, 'immediate')

stop_servers(gather_severs_connected_to_node(get_cells))

#!/usr/bin/env python3

from ansible.module_utils.basic import AnsibleModule
import xml.etree.ElementTree as etree
import pwd
import os
import zipfile

#my first Ansible Module - be gentle and do not judge it
DOCUMENTATION = '''
---
module: iim_updater 
description:
     - The M(iim_updater) module automatically installs updates for components of Installation Manager. It validates XML files, compares the result, updates only packages which are installed and possible to update (compares its version) and calls imcl commands only for update. Even though that the "path" to a package with an update is not a mandatory option, it is expected that either the 'path_to_update_iim_xml' or the 'path_to_iim_interim_location' exists; it would be impossible to update anything without at least one of the paths.

options:
  path_to_iim_xml:
    description:
        This is the path to installed.xml file created by The "IBM's Installation Manager.
        By default it is stored in $HOME directory of a user who performed the installation.
        With the exception of user root and custom specification of a path.
    required: true
  path_to_update_iim_xml:
    description:
        This is a path to a repository which holds the repository.xml. This file is provided by a package for update (assuming offline update).
        Files are usually several gigs in size and split into multiple zip files.
        This path must point to "repository.xml" which is extracted from above zips.
    required: false
  path_to_iim_interim_location:
    description:
      This is a path to a ZIP file for the interim FIX. These files are usually quick fixes for CVEs and are only few MB in size.
    required: false
  path_iim_location:
    description:
      Path to the location of Installation Manager executable imcl command
    required: true
  admin_user:
    description:
      The user who performed the installation of packages via Installation Manager.
      This is a check which validates if "become_user" == admin_user. Otherwise the Installation Manager
      would fail or corrupt already installed files.
    required: true

author:
    - "Karel 'def' Vampola"

notes:
    - Lampros was eager to help me but yet ... :)
'''
# some of the functionality (e.g. reading of zip files) can be achieved by the Installation Manager itself, but the overall idea
# is to speed up the process and call it for installation only
# + provide quick and understanble interface for users who never updated product using Installation Manager 
# additional optional parameters will probably follow, didn't decide yet

EXAMPLES = '''
To install FixPack
- was_fix:
    path_to_iim_xml: /home/my_user/var/ibm/InstallationManager/installed.xml
    path_to_update_iim_xml: /tmp/Downloads/unzipped/repository.xml
    path_iim_location: /opt/IBM/IIM/eclipse/tools/imcl
    admin_user: my_user

To install InterimFix
- was_fix:
    path_to_iim_interim_location:/tmp/update.zip
    path_to_iim_xml: /home/my_user/var/ibm/InstallationManager/installed.xml
    path_iim_location: /opt/IBM/IIM/eclipse/tools/imcl
    admin_user: my_user

Empty dict() is returned if it is not possible to update any package.

'''


def get_info_installed_files(path_to_iim_xml):

    components_installed_dict = {}
    # is a check for WIN necessary? Who would install WebSphere on WIN? :)
    tree = etree.parse(path_to_iim_xml)
    root = tree.getroot()

    for one_location in root.findall("location"):
        for one_package in one_location.findall("package"):
            #product_name = one_package.get("name")
            product_installed_id = one_package.get("id")
            product_installed_version = one_package.get("version")
            product_path = one_location.get("path")
            # create a long "int" from the version of a package -> 8.5.5.11_0000 => 85511... 
            # - handled in compare_xml_files func
            # product_installed_version_sub = product_installed_version.split('_')[0].replace('.', '')

            #was_checked_version = re.match(r'\d\.\d\.\d+', was_installed_version)
            # if was_checked_version:....

            # return the installed packages as a dict => Product: ID; e.g.
            #com.ibm.websphere.BASE.v85: 85501620190801
            components_installed_dict[product_installed_id] = product_installed_version, product_path
    return (components_installed_dict)


def main():

    custom_module = AnsibleModule(
        argument_spec={
            "path_to_iim_xml": {"required": True, "type": "path"},
            "path_to_update_iim_xml": {"required": False, "type": "path"},
            "path_iim_location": {"required": True, "type": "path"},
            "admin_user": {"required": True, "type": "str"},
            "path_to_iim_interim_location" :{"required": False, "type": "path"}
        }
    )

    path_to_iim_xml = custom_module.params["path_to_iim_xml"]
    path_to_update_iim_xml = custom_module.params["path_to_update_iim_xml"]
    path_iim_location = custom_module.params["path_iim_location"]
    admin_user = custom_module.params["admin_user"]
    path_to_iim_interim_location = custom_module.params["path_to_iim_interim_location"]


    if not os.path.isfile(path_to_iim_xml):
        custom_module.fail_json(
            msg="The file %s does not exist" % (path_to_iim_xml))


    if not os.path.isfile(path_iim_location):
        custom_module.fail_json(
            msg="The file %s does not exist" % (path_iim_location))

    if not admin_user:
        custom_module.fail_json(
            msg="The username %s can not be empty" % (admin_user))

    # check if the admin_user really owns the imcl executable
    iim_owner = pwd.getpwuid(os.stat(path_iim_location).st_uid).pw_name
    if iim_owner != admin_user:
        custom_module.fail_json(
            msg="The owner of imcl application is -> %s it must be as same as admin_user -> %s" % (iim_owner, admin_user))

    installation_manager_info = dict(
        path_to_iim_xml=path_to_iim_xml,
        path_iim_location=path_iim_location, admin_user=admin_user
    )

    if path_to_update_iim_xml is not None:
        if not os.path.isfile(path_to_update_iim_xml):
            custom_module.fail_json(msg="The file %s does not exist" % (path_to_update_iim_xml))

        def compare_xml_files(path_to_update_iim_xml):
            tree = etree.parse(path_to_update_iim_xml)
            root = tree.getroot()

            available_components = {}
            for one_repo in root.findall("repository"):
                for one_element in one_repo.findall("offering"):
                    new_product_version = one_element.get("version")
                    new_product_version_sub = new_product_version.split("_")[
                        0].replace(".", '')
                    old_product_version = get_info_installed_files(
                        path_to_iim_xml).get(one_element.get("id"))

                    if old_product_version is not None:
                        old_product_version = old_product_version[:][0]
                        old_product_version_sub = old_product_version.split("_")[0].replace(".", '')

                    if (one_element.get("id") in get_info_installed_files(path_to_iim_xml).keys() and
                            int(old_product_version_sub) < int(new_product_version_sub)):
                        available_components[one_element.get("id")] = one_element.get(
                            "id")+"_"+new_product_version
                return(available_components)
        
        validate_xml = dict(
        path_to_repo_FIX=path_to_update_iim_xml,
        installed_packages=get_info_installed_files(path_to_iim_xml),
        packages_for_update=compare_xml_files(path_to_update_iim_xml),
        )

        custom_module.exit_json(info_iim=installation_manager_info, validate_installation=validate_xml)
            

    if path_to_iim_interim_location is not None:
        if not os.path.isfile(path_to_iim_interim_location):
            custom_module.fail_json(msg="The file %s does not exist" % (path_to_iim_interim_location))

        def interim_fix_info(path_to_iim_interim_location):
            interim_fix_dict = {}
            if zipfile.is_zipfile(path_to_iim_interim_location):
                with zipfile.ZipFile(path_to_iim_interim_location, 'r') as interim_zip:
                    with interim_zip.open('repository.xml', 'r') as repo_in_zip:
                        tree = etree.parse(repo_in_zip)
                        root = tree.getroot()
                        
                        repo = root.find("repository")

                        fix_info = repo.find("fix").attrib
                        package_name = {"interim_fix_name":fix_info['id']+'_'+fix_info['version']}
                        
                        for one_property in repo.iter("property"):
                            for one_value in one_property.attrib.values():
                                for one_key in get_info_installed_files(path_to_iim_xml).keys():
                                    if one_key in one_value:
                                        interim_fix_dict[one_key] = (one_key+'_'+get_info_installed_files(path_to_iim_xml)[one_key][:][0], get_info_installed_files(path_to_iim_xml)[one_key][:][1])
            return(package_name, interim_fix_dict)
        
        validate_interim = dict(
        interim_can_be_applied_to_packages=interim_fix_info(path_to_iim_interim_location),
        path_to_interimFix = path_to_iim_interim_location,
        )

        custom_module.exit_json(info_iim=installation_manager_info, validate_interim=validate_interim)

    no_packages = dict(
        no_packages_provided="Please provide either path for FIX or InterimFIX repository"
    )

    custom_module.exit_json(info_iim=installation_manager_info, no_packages=no_packages)

if __name__ == '__main__':
    main()